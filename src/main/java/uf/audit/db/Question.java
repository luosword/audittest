package uf.audit.db;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

import uf.audit.util.Table;

/**
 * 考题数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "itembh")
public class Question extends Model<Question> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static Question dao = new Question().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Question set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取某个序列/分类或者所有的题目信息
	public List<Question> getquestions(String seqbh, String catebh) {
		return getquestions(seqbh, catebh, null, null);
	}

	// 获取某个序列/分类/类型/难度或者所有的题目信息
	public List<Question> getquestions(String seqbh, String catebh, String itemtype, Integer itemlevel) {
		String sql = "select * from question where 1=1 ";
		if (seqbh != null) {
			sql += " and itemseq='" + seqbh + "' ";
		}
		if (catebh != null) {
			sql += " and itemcate='" + catebh + "' ";
		}
		if (itemtype != null) {
			sql += " and itemtype='" + itemtype + "' ";
		}
		if (itemlevel != null && itemlevel != 0 /* 这里处理前端返回的默认0 */) {
			sql += " and itemlevel=" + itemlevel + " ";
		}
		return find(sql);
	}

	public Question getquestionbycontent(String content, String seq, String cate) {
		String sql = "select * from question where itemcontent=? and itemseq=? and itemcate=?";
		return findFirst(sql, content, seq, cate);
	}

	// 更新
	public String update(JSONObject obj) {
		String bh = obj.getString("itembh");
		Question item = new Question();
		item.set("itemseq", obj.getString("itemseq"));
		item.set("itemcate", obj.getString("itemcate"));
		item.set("itemcontent", obj.getString("itemcontent"));
		item.set("itemlevel", obj.getInteger("itemlevel"));
		item.set("itemtype", obj.getString("itemtype"));
		// item.set("itemcreate", obj.getString("itemcreate"));
		String result = bh;
		if (bh != null) {
			item.set("itembh", obj.getString("itembh"));
			if (!item.update()) {
				result = null;
			}
		} else {
			result = UUID.randomUUID().toString();
			item.set("itembh", result);
			if (!item.save()) {
				result = null;
			}
		}
		return result;
	}

	// 删除
	public boolean delete(final String bh) {
		return DbPro.use().tx(new IAtom() {

			public boolean run() throws SQLException {
				boolean result = deleteById(bh);
				result = result && (Options.dao.deleteWithItembh(bh));
				return result;
			}
		});
	}

	public int getTestItemCount(String itemlevel, String seqbh, String itemtype, String catebh) {
		String sql = "select count(*) as TOTAL from question where 1=1 ";
		if (itemlevel != null)
			sql += " and (itemlevel = " + itemlevel + ") ";
		if (seqbh != null)
			sql += " and (itemseq='" + seqbh + "')";
		if (catebh != null)
			sql += " and (itemcate='" + catebh + "')";
		if (itemtype != null)
			sql += " and (itemtype='" + itemtype + "')";
		Question rec = dao.findFirst(sql);
		if (rec == null)
			return 0;
		return rec.getLong("TOTAL").intValue();
	}

	public boolean deleteQuestions(final String[] ids) {
		return DbPro.use().tx(new IAtom() {

			public boolean run() throws SQLException {
				for (String id : ids) {
					DbPro.use().update("delete from options where itembh=?", id);
					DbPro.use().update("delete from question where itembh=?", id);
				}
				return true;
			}
		});
	}
}
