package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;
import uf.audit.util.Utils;

/**
 * 文档管理实体
 * 
 * @author sunny
 *
 */
@Table(key = "docbh")
public class Docs extends Model<Docs> {
	private static final long serialVersionUID = -7644540082576539660L;
	public static Docs dao = new Docs().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Docs set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取所有的文档
	public List<Docs> getdocs() {
		return find("select * from docs order by createtime desc");
	}

	// 更新
	public boolean update(String docbh, String docname, String filetoken, String creater) {
		Docs item = new Docs();
		item.set("docname", docname);
		item.set("creater", creater);
		item.set("createtime", Utils.getDateTime());
		if (filetoken != null)
			item.set("filetoken", filetoken);
		boolean result = false;
		if (docbh != null) {
			item.set("docbh", docbh);
			result = item.update();
		} else {
			item.set("docbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		DbPro.use().update("delete from docs where docbh=?", bh);
		return deleteById(bh);
	}
}
