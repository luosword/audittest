package uf.audit.db.support;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

import uf.audit.util.AuditEntities;

/**
 * 实体注册
 * 
 * @author sunny
 *
 */
public class EntityRegister {
	private static final Logger log = Logger.getLogger(EntityRegister.class);
	private ActiveRecordPlugin arpPlugin;

	public EntityRegister(ActiveRecordPlugin arp) {
		arpPlugin = arp;
	}

	public void regist() {
		// arpPlugin.addMapping("marking", "markingbh", Marking.class);
		try {
			AuditEntities ae = new AuditEntities(arpPlugin); // 根据注解自动注册bean
			ae.config();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		log.info("entities registed");
	}
}
