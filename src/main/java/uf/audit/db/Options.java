package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 答案实体
 * 
 * @author sunny
 *
 */
@Table(key = "optbh")
public class Options extends Model<Options> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static Options dao = new Options().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Options set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取某个题目（或者全部）的答案选项列表
	public List<Options> getoptions(String itembh) {
		String sql = "select * from options where 1=1 ";
		if (itembh != null) {
			sql += " and itembh='" + itembh + "' ";
		}
		sql += " order by optno asc";
		return find(sql);
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("optbh");
		Options item = new Options();
		item.set("itembh", obj.getString("itembh"));
		item.set("optcontent", obj.getString("optcontent"));
		item.set("optno", obj.getString("optno"));
		item.set("isanswer", obj.getInteger("isanswer"));
		boolean result = false;
		if (bh != null) {
			item.set("optbh", obj.getString("optbh"));
			result = item.update();
		} else {
			item.set("optbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}

	// 删除指定题目编号的所有的选项
	public boolean deleteWithItembh(final String bh) {
		return DbPro.use().update("delete from options where itembh=?", bh) > 0;
	}
}
