package uf.audit.config;

import com.jfinal.aop.Interceptor;
import com.jfinal.config.*;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.render.ViewType;
import uf.audit.db.support.ACDBCPPlugin;
import uf.audit.db.support.EntityRegister;
import uf.audit.intercept.AuthInterceptor;
import uf.audit.util.AuditRoutes;
import uf.audit.util.Consts;

import java.sql.Connection;

/**
 * JFinal配置文件，由于JFinal采用coc范式，没有大量的spring xml配置，在小型系统比较方便，大型系统建议还是采用配置方式
 *
 * @author sunny
 */
public class AuditConfig extends JFinalConfig {

    @Override
    public void configConstant(Constants consts) {
        // 配置常量
        consts.setEncoding(Consts.DEFAULT_ENCODING);
        consts.setViewType(ViewType.VELOCITY);
        consts.setVelocityViewExtension("html");
        consts.setError500View("/error.html");
        consts.setError401View("/401.html");
        consts.setError404View("/40x.html");
        int maxPostSize = 1024 * 1024 * 100; // 100M
        consts.setMaxPostSize(maxPostSize);
    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    @Override
    public void configInterceptor(Interceptors intercepts) {
        // 配置拦截器（这里预置了权限拦截器）
        Interceptor authInterceptor = new AuthInterceptor();
        intercepts.add(authInterceptor);
    }

    @Override
    public void configPlugin(Plugins plugins) {
        // 配置插件（数据库访问插件在这里配置）
        ACDBCPPlugin dbpool; // 数据库连接池插件
        try {
            dbpool = (ACDBCPPlugin) ACDBCPPlugin
                    .buildFromProperties("/db.properties");
            dbpool.start();
            plugins.add(dbpool);
            ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(
                    dbpool); // activeRecord插件（借鉴了ror中的概念）
            activeRecordPlugin.setDevMode(true);
            activeRecordPlugin
                    .setTransactionLevel(Connection.TRANSACTION_SERIALIZABLE);
            activeRecordPlugin.setDialect(new SqlServerDialect());
            // 实体注册
            EntityRegister reg = new EntityRegister(activeRecordPlugin);
            reg.regist();
            activeRecordPlugin.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void configRoute(Routes routers) {
        // 访问路由设置
        String scanPackages = "uf.audit.controller";
        AuditRoutes auditRoutes = new AuditRoutes();
        auditRoutes.scanPackage(scanPackages);
        routers.add(auditRoutes);
    }
}
