package uf.audit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlUtil {

	public static Document loadXmlDocument(String filePath) throws Exception {
		Document document = null;
		InputStream is = null;
		java.io.Reader reader = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			is = new FileInputStream(new File(filePath));
			reader = new InputStreamReader(is, Consts.DEFAULT_ENCODING);
			InputSource inputSource = new InputSource(reader);
			document = builder.parse(inputSource);
			document.normalize();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return document;
	}

	public static Document loadXmlDocument(InputStream is) throws Exception {
		Document document = null;
		java.io.Reader reader = null;
		if (is == null)
			return null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			reader = new InputStreamReader(is, Consts.DEFAULT_ENCODING);
			InputSource inputSource = new InputSource(reader);
			document = builder.parse(inputSource);
			document.normalize();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return document;
	}

	/**
	 * 查找指定单个节点
	 * 
	 * @param express
	 * @param source
	 * @return
	 */
	public static Node selectSingleNode(String express, Object source) {
		Node result = null;
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();
		try {
			result = (Node) xpath.evaluate(express, source, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 查找节点集
	 * 
	 * @param express
	 * @param source
	 * @return
	 */
	public static NodeList selectNodes(String express, Object source) {
		NodeList result = null;
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();
		try {
			result = (NodeList) xpath.evaluate(express, source, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) {
		LinkedHashMap<String, HashMap<String, String>> hashMap = new LinkedHashMap<String, HashMap<String, String>>();
		try {
			Document xmlDocument = loadXmlDocument(
					"E:\\MyWork\\tax_bj\\tax-inspect\\tax\\src\\main\\resources\\checkColumn.xml");
			NodeList nodeList = selectNodes("/tableConfig/table", xmlDocument);
			for (int index = 0, length = nodeList.getLength(); index < length; index++) {
				Node curNode = nodeList.item(index);
				if (Node.ELEMENT_NODE == curNode.getNodeType()) {
					Element element = (Element) curNode;
					String tableName = element.getAttribute("tableName");
					NodeList subNodeList = selectNodes(
							String.format("/tableConfig/table[@tableName='%s']/column", tableName), xmlDocument);
					if (subNodeList != null && subNodeList.getLength() > 0) {
						HashMap<String, String> subNodeMap = new HashMap<String, String>();
						for (int i = 0, len = subNodeList.getLength(); i < len; i++) {
							Node curSubNode = subNodeList.item(i);
							if (Node.ELEMENT_NODE == curSubNode.getNodeType()) {
								Element subElement = (Element) curSubNode;
								subNodeMap.put(subElement.getAttribute("columnName"),
										subElement.getAttribute("columnType"));
							}
						}
						if (!subNodeMap.isEmpty()) {
							hashMap.put(tableName, subNodeMap);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (Iterator<String> it = hashMap.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			HashMap<String, String> map = hashMap.get(key);

			System.out.println(key + " && " + map);
		}

	}

}
